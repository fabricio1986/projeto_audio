
import numpy as np
import pandas as pd
import os
import librosa
import librosa.display
import matplotlib.pyplot as plt
from scipy.stats import skew
from tqdm import tqdm
tqdm.pandas()
from sklearn import preprocessing
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier

from spectrum import get_spectrum
from math import fabs
from sklearn import tree
import scipy
import autosklearn.classification
from sklearn.linear_model import RidgeClassifier
from sklearn import linear_model
import numpy
from funcoes_atributos import *


TRAINING_OUTPUT = '/home/fabricio/Desktop/projetoaudio/output_train/'
TRAINING_AUDIO_CAPTCHA_FOLDERS = [TRAINING_OUTPUT+i for i in os.listdir(TRAINING_OUTPUT)]
TRAINING_AUDIO_FILENAMES = [] # -> <number>_<digit>.wav
for folder in TRAINING_AUDIO_CAPTCHA_FOLDERS:
    for f in os.listdir(folder):
        TRAINING_AUDIO_FILENAMES.append(folder+'/'+f)

TEST_OUTPUT = '/home/fabricio/Desktop/projetoaudio/output_test/'
TEST_AUDIO_CAPTCHA_FOLDERS=[]
for folder in os.listdir(TEST_OUTPUT):
    TEST_AUDIO_CAPTCHA_FOLDERS.append(TEST_OUTPUT+folder)


SAMPLE_RATE = 44100
def extract_features(audio_filename: str, path: str) -> pd.core.series.Series:
    data, _ = librosa.core.load(path + audio_filename, sr=SAMPLE_RATE)
    assert _ == SAMPLE_RATE
    try:
        label = audio_filename.split('.')[0].split('-')[-1]
        
        
        
        data = librosa.util.normalize(data)
        janela = int(round(len(data)/4.8))
        if janela%2 == 0:
            janela += 1
        data1_positivo = list(map(fabs,data))
        ydata3 = scipy.signal.savgol_filter(data1_positivo,janela,1)
        
        janela = int(round(len(data)/1084))
        ydata3 = suave_verifica(ydata3,janela)
             
        j_esq = corte_esq(ydata3)
        j_dir = corte_esq(list(reversed(ydata3)))
        
        data = data[j_esq:len(data)-j_dir]
        
        
        
        
        
        ft1_raw = librosa.feature.mfcc(data, sr=SAMPLE_RATE, n_mfcc=40)
        ft1 = np.array([list(map(fabs, sublist)) for sublist in ft1_raw]) # Tudo positivo
        npstd = np.std(ft1, axis=1)
        npmedian = np.median(ft1, axis=1)
        delta = np.max(ft1) - np.min(ft1)
        delta2 = np.max(ft1,axis=1) - np.min(ft1,axis=1)
        ft1_trunc = np.hstack((npmedian, npstd, delta,delta2))
        
        ft2 = librosa.feature.zero_crossing_rate(y=data)
        ft2_trunc = np.mean(ft2)    
        
        
        '''
        janela = int(len(data)/40)
        
        cent = librosa.feature.spectral_centroid(y=data, sr=SAMPLE_RATE,hop_length=janela)    
        
        rmse = librosa.feature.rmse(y=data,hop_length=janela)
        spec_bw = librosa.feature.spectral_bandwidth(y=data,hop_length=janela)
        flatness = librosa.feature.spectral_flatness(y=data,hop_length=janela)
        tempo, beats = librosa.beat.beat_track(y=data, hop_length=200)
        if (len(beats)==0):
            beats=0
        onset = librosa.beat.onset.onset_strength(y=data,hop_length=janela)
        rolloff = librosa.feature.spectral_rolloff(y=data, hop_length=janela)
        harm_abs = librosa.effects.harmonic(y=data1)
        harm = librosa.effects.harmonic(y=data)
        tonnetz = librosa.feature.tonnetz(y=data)
        pic = numero_picos(picos(ydata31),ydata3)
        mel = librosa.feature.melspectrogram(y=data)[:15,:]
        
        '''
        mel_raw = librosa.feature.melspectrogram(y=data)[:20,:]
        mnpstd = np.std(mel_raw, axis=1)
        mnpmedian = np.median(mel_raw, axis=1)
        mdelta = np.max(mel_raw) - np.min(mel_raw)
        mdelta2 = np.max(mel_raw,axis=1) - np.min(mel_raw,axis=1)
        mel_trunc = np.hstack((mnpmedian, mnpstd, mdelta,mdelta2))
        
        
        
        ###############################################
        zero = stZCR(data)
        energia = stEnergy(data)
        entro = stEnergyEntropy(data)
        spect = stSpectralEntropy(data)
        har = stHarmonic(data,fs=44000)
        ###############################################
            
        #plt.figure(figsize=(10, 4))
        #librosa.display.specshow(ft1, x_axis='time')
        #plt.colorbar()
        #plt.title(label)
        #plt.tight_layout()
        #plt.show()
        #caract = np.hstack([get_spectrum(data),delta2, mdelta2,npmedian, npstd, delta,mnpmedian, mnpstd, mdelta,zero,energia,entro,spect,ft2_trunc,har[0],har[1],np.mean(cent),np.std(cent),rmse,spec_bw,np.mean(spec_bw),np.std(spec_bw)])
        #features = pd.Series(np.hstack((ft1_trunc[pp], ft2_trunc, get_spectrum(data),np.mean(onset),np.mean(cent),np.mean(rmse),np.std(rmse),np.std(onset),np.std(cent),np.std(rolloff),np.mean(rolloff),np.std(harm),np.mean(harm),np.std(harm_abs),np.mean(harm_abs),np.std(tonnetz,axis = 1),np.mean(tonnetz,axis=1),pic[0],pic[1],pic[2],pic[3],pic[4],mel_trunc,np.std(tonnetz,axis = 1),np.mean(tonnetz,axis=1),np.std(flatness,axis = 1),np.mean(flatness,axis=1),label)))
        #features = pd.Series(np.hstack((ft1_trunc, ft2_trunc, get_spectrum(data),ft2_trunc*get_spectrum(data)[-1],ft2_trunc*get_spectrum(data)[-2],pic[1],pic[3]*pic[4],pic[3],pic[4],label)))
        features = pd.Series(np.hstack((zero,energia,entro,spect,delta2, mdelta2,ft2_trunc,get_spectrum(data),label)))
        
        
        return features
    except:
        print(path + audio_filename)
        return pd.Series([0]*210)


def train() -> tuple:
    X_train_raw = []
    y_train = []
    for sample in TRAINING_AUDIO_FILENAMES:
        folder = '/home/fabricio/Desktop/projetoaudio/output_train/'+sample.split('/')[-2]+'/'
        filename = sample.split('/')[-1]
        obj = extract_features(filename, folder)
        d = obj[0:obj.size - 1]
        l = obj[obj.size - 1]
        X_train_raw.append(d)
        y_train.append(l)

    # Normalise
    
    std_scale = preprocessing.StandardScaler().fit(X_train_raw) 
    X_train = std_scale.transform(X_train_raw)
    
    return X_train, y_train, std_scale


def test(X_train: np.ndarray, y_train: np.ndarray, std_scale: preprocessing.data.StandardScaler):
    accuracyNB = 0
    accuracy1NN = 0
    accuracy3NN = 0
    accuracySVM = 0
    accuracyQDA = 0
    accuracyLDA = 0
    accuracyMLP=0
    accuracytre=0
    accuracyautoml = 0
    accuracyrid=0
    accuracyreg = 0
    
    #gnb = GaussianNB().fit(X_train, y_train)
    #QDA = QuadraticDiscriminantAnalysis().fit(X_train, y_train)
    #LDA = LinearDiscriminantAnalysis().fit(X_train, y_train)
    #MLP = MLPClassifier().fit(X_train, y_train)
    #neigh3 = KNeighborsClassifier(n_neighbors=3).fit(X_train, y_train)
    #tre = tree.DecisionTreeClassifier().fit(X_train, y_train)
    #rid = RidgeClassifier().fit(X_train, y_train)
    #reg = linear_model.Ridge (alpha = .5).fit(X_train, y_train)
    #automl = autosklearn.classification.AutoSklearnClassifier(    include_estimators=["random_forest", ], exclude_estimators=None,     include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None).fit(X_train, y_train)
    
    neigh1 = KNeighborsClassifier(n_neighbors=1).fit(X_train, y_train)
    clf = SVC().fit(X_train, y_train)
    
    X_tes_raw = []
    y_tes = []
    matsvm=[]
    matknn=[]
    correto=[]
    for folder in TEST_AUDIO_CAPTCHA_FOLDERS:
        correctNB = 0
        correct1NN = 0
        correct3NN = 0
        correctSVM = 0
        correctQDA = 0
        correctLDA = 0
        correctMLP = 0
        correcttre = 0
        correctautoml = 0
        correctrid=0
        correctreg=0
        folder = folder+'/'
        correto_aux=[]
        for filename in os.listdir(folder):
            obj = extract_features(filename, folder)
            y_test = obj[obj.size - 1]
            X_test_raw = [obj[0:obj.size - 1]]
            X_test = std_scale.transform(X_test_raw) # normalise
            X_tes_raw.append(X_test)
            y_tes.append(y_test)
            
            
            
            
            y_pred = neigh1.predict(X_test)
            matknn.append((y_pred[0],y_test))
            if y_pred[0] == y_test:
                correct1NN+=1
            y_pred = clf.predict(X_test)
            matsvm.append((y_pred[0],y_test))
            if y_pred[0] == y_test:
                correctSVM+=1
            
            '''
            y_pred = gnb.predict(X_test)
            if y_pred[0] == y_test:
                correctNB+=1
           
            y_pred = neigh3.predict(X_test)
            if y_pred[0] == y_test:
                correct3NN+=1
            
            y_pred = QDA.predict(X_test)
            if y_pred[0] == y_test:
                correctQDA+=1
                
            y_pred = LDA.predict(X_test)
            if y_pred[0] == y_test:
                correctLDA+=1
            y_pred = MLP.predict(X_test)
            if y_pred[0] == y_test:
                correctMLP+=1
                
            y_pred = tre.predict(X_test)
            if y_pred[0] == y_test:
                correcttre+=1
            
            y_pred = automl.predict(X_test)
            if y_pred[0] == y_test:
                correctautoml+=1
            
            y_pred = rid.predict(X_test)
            if y_pred[0] == y_test:
                correctrid+=1
                
            y_pred = reg.predict(X_test)
            y_pred[0] = dist(y_pred[0])
            if y_pred[0] == y_test:
                correctreg+=1
            '''    
        
        if correct1NN == 4:
            accuracy1NN+=1
        correto_aux.append(correct1NN)
        
        if correctSVM == 4:
            accuracySVM+=1
        correto_aux.append(correctSVM)
        '''
        if correctNB == 4:
            accuracyNB+=1
        
        if correct3NN == 4:
            accuracy3NN+=1
        correto_aux.append(correct3NN)
                      
        if correctrid == 4:
            accuracyrid+=1
        
        if correctreg == 4:
            accuracyreg += 1            
        
        if correctQDA == 4:
            accuracyQDA+=1
        if correctLDA == 4:
            accuracyLDA+=1
        if correctMLP == 4:
            accuracyMLP+=1
        if correcttre == 4:
            accuracytre+=1
        
        if correctautoml == 4:
            accuracyautoml+=1
        ''' 
        correto.append(correto_aux)
    print("Acuracia 1NN = "+str(accuracy1NN / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    print("Acuracia SVM = "+str(accuracySVM / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    '''
    print("Acuracia 3NN = "+str(accuracy3NN / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    print("Acuracia RID = "+str(accuracyrid / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    print("Acuracia REG = "+str(accuracyreg / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    print("Acuracia Naive Bayes = "+str(accuracyNB / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    print("Acuracia QDA = "+str(accuracyQDA / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    print("Acuracia LDA = "+str(accuracyLDA / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    print("Acuracia MLP = "+str(accuracyMLP / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    print("Acuracia tre = "+str(accuracytre / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    print("Acuracia automl = "+str(accuracyautoml / len(TEST_AUDIO_CAPTCHA_FOLDERS)))
    '''
    
    return matsvm,accuracySVM / len(TEST_AUDIO_CAPTCHA_FOLDERS), matknn,accuracy1NN / len(TEST_AUDIO_CAPTCHA_FOLDERS),correto


def important_features() -> np.ndarray:
    """Retorna um array com as features mais importantes,
    extraidas a partir da base de treino.
    """
    X, Y, std_scale = train()
    rnd_clf = RandomForestClassifier(n_estimators=500, n_jobs=-1, random_state=42)
    rnd_clf.fit(X, Y)
    print(rnd_clf.feature_importances_)
    return rnd_clf.feature_importances_, X


def break_captcha():
    X_train, y_train,  std = train()
    return test(X_train, y_train,  std)

if __name__ == "__main__":
    resultado = break_captcha()
    print('Matrix de confus�o - 1NN \n \n')
    matrix_conf(resultado[2])
    print('Matrix de confus�o - SVM \n \n')
    matrix_conf(resultado[0])
